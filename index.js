var through = require('through2')
var RapydScript = require('rapydscript-ng').create_compiler()
var fs = require('fs')
var path = require('path')

function isRapydscript(file) {
	return /\.pyj?$/.test(file)
}

module.exports = function(file, opts) {
	if (isRapydscript(file)) {
		var data = ''

		var stream = through(function write(chunk, enc, cb) {
			data += chunk
			cb()
		}, function end(cb) {
			try {
				var output = RapydScript.parse(data,
                                    {    filename:file,
                                         toplevel: null,
                                         base_dir: ".",
                                         import_dirs: [path.dirname(file)],
                                         auto_bind: true,
                                         libdir: 'node_modules/rapydscript/src/lib',
                                         readfile:fs.readFileSync,
                                         omit_baselib:false,
                                         private_scope:false,
                                         beautify:true } )
			} catch (e) {
				console.error(e.message)
				cb(e)
				return
			}
			var output_options = {
				omit_baselib:true,
				write_name:false,
				private_scope:false,
				beautify:true,
				js_version: 6,
				keep_docstrings:true }
			var output_stream = new RapydScript.OutputStream(output_options)
			output.print(output_stream)
			this.push(output_stream)
			cb()
		})

		return stream
	} else {
		return through()
	}
}
