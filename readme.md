Require Python-like [RapydScript-NG](https://github.com/kovidgoyal/rapydscript-ng) code in your [browserify](https://github.com/substack/node-browserify)-built webapp!

## To use

Include `rapydscriptify-ng` in your browserify-using package:

```sh
npm install rapydscriptify-ng --save-dev
```

Change your build to use the rapydscriptify-ng transform:

```sh
browserify -t rapydscriptify-ng start.js -o coolbuild.js
```

All files with the extension `.pyj` or `.py` will be interpreted as RapydScript-NG files.

## Lineage

This script is modified from [lispyscriptify](https://github.com/TehShrike/lispyscriptify) which, naturally, is the Lispyscript transform for Browserify.

The original modification was made for [RapydScript](http://rapydscript.com/).
